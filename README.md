# Lip pattern recognition

Human lips have a number of elevation and depressions features called lip prints and examination of lip prints is referred to cheiloscopy. Such patterns are very similar to fingerprint, iris or palm print patterns.

Lip print characteristics have been widely used in forensics by experts and in criminal police practice for human identification. The grooves in the human lips are unique to each person and are used to determine human identification. Lip prints are also used to support the sex determination of the examined subject.

Identifying lip features will be useful both in criminal forensics and personal identification.

# Requirements

- Python 3 + virtualenv
- CMake, Boost

To execute:
- `$ virtualenv venv`
- `$ source venv/bin/activate`
- `$ pip install -r requirements.txt`

Any file can be executed as

`$IMAGE=path/to/file python pyton_file.py`

## Lip patterns

Takes as input a print of lips. Detects grooves. Saves image resulting from top hat transformation and Canny edge detection algorithm.

## Photo normalization

Takes as input a photo of lips. Detects lips, tries to save grooves. Saves image resulting from filters.

## Matching

Takes two images and performs ORB matching

## DTW

Takes two black and white images, and performs DTW algorithm to compute similarity.

## Main

Ask for print and photo images. Will perform all steps above.

# Contributors

- Uriel Ochoa
- Alejandra Coloapa

Work under supervision from the UNAM Forensics Laboratory.
