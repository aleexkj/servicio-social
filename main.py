
import tkinter as tk
from tkinter import filedialog
from lip_patterns import print_to_lines
from photo_norm import photo_to_lines
from dtw import compare
from matching import draw_kp
import cv2 as cv

root = tk.Tk()
root.withdraw()

file_path_print = filedialog.askopenfilename()
file_path_photo = filedialog.askopenfilename()

# Process
print_to_lines(file_path_print)
photo_to_lines(file_path_photo)

# Load processed
print_lower = cv.imread('lower_lip_tophat.png', 0)
print_upper = cv.imread('upper_lip_tophat.png', 0)
photo_lower = cv.imread('lower_lip_grooves.png', 0)
photo_upper = cv.imread('upper_lip_grooves.png', 0)

# DTW
print('LOWER LIP DTW')
compare(photo_lower, print_lower)
print('UPPER LIP DTW')
compare(photo_upper, print_upper)

# Matching
print('LOWER LIP ORB')
draw_kp(photo_lower, print_lower)
print('UPPER LIP ORB')
draw_kp(photo_upper, print_upper)

