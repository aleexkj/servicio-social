# DTW algorithm
import numpy as np
import cv2 as cv

import tkinter as tk
from tkinter import filedialog

def calculate_dtw(x, y):
  N = y.shape[0]
  M = x.shape[0]
  Dnm = np.zeros((N, M))

  print('DTW of shape', Dnm.shape)

  for i in range(N):
    for j in range(M):
      Dnm[i, j] = (x[j]-y[i])**2

  Ac = np.zeros((N, M))
  Ac[0, 0] = Dnm[0, 0]

  for j in range(1, M):
    Ac[0, j] = Dnm[0, j] + Ac[0, j - 1]

  for i in range(1, N):
    Ac[i, 0] = Dnm[i, 0] + Ac[i - 1, 0]

  for i in range(1, N):
    for j in range(1, M):
      Ac[i, j] = Dnm[i, j] + np.array([
        Ac[i - 1, j],
        Ac[i, j - 1],
        Ac[i - 1, j - 1]
      ]).min()

  # Dnm has the distances btw q, u points
  # print(Dnm)
  W = [[M - 1, N - 1]]
  i = N - 1
  j = M - 1

  while i > 0 and j > 0:
    if i == 0:
      j = j - 1
    elif j == 0:
      i = i - 1
    else:
      the_min = np.array([
        Ac[i - 1, j],
        Ac[i, j - 1],
        Ac[i - 1, j - 1]
      ]).min()

      if Ac[i - 1, j] == the_min:
        i = i - 1
      elif Ac[i, j - 1] == the_min:
        j = j - 1
      else:
        i = i-1
        j = j-1

    W.append([j, i])

  W.append([0, 0])

  distance = 0
  for cell in W:
    distance += Dnm[cell[1]][cell[0]]

  return distance


def calculate_histos(src):
  ROWS = src.shape[0]
  COLS = src.shape[1]
  bins_rows = np.zeros((ROWS,))
  bins_cols = np.zeros((COLS,))
  bins_45 = np.zeros((ROWS + COLS))
  bins_135 = np.zeros((ROWS + COLS))
  n_rows = ROWS - 1
  n_cols = COLS - 1

  # horizontal
  for row in range(ROWS):
    bins_rows[row] = np.bincount(src[row])[0]

  # vertical
  tophat_transpose = src.T
  for col in range(COLS):
    bins_cols[col] = np.bincount(tophat_transpose[col])[0]

  # 45 diagonal
  tophat_flip = np.fliplr(src)
  index = 0
  for offset in range(n_cols, -n_rows, -1):
    diagonal = np.diagonal(tophat_flip, offset)
    bins_45[index] = np.bincount(diagonal)[0]
    index += 1

  # 135 diagonal
  index = 0
  for offset in range(-n_rows, n_cols, 1):
    diagonal = np.diagonal(src, offset)
    bins_135[index] = np.bincount(diagonal)[0]
    index += 1

  return bins_rows, bins_cols, bins_45, bins_135


def compare(src_a, src_b):
  src_a = cv.resize(src_a, (src_b.shape[1], src_b.shape[0]), interpolation=cv.INTER_AREA)
  
  
  histos_a = calculate_histos(src_a)
  histos_b = calculate_histos(src_b)
  names = ['Horizontal', 'Vertical', '45', '135']

  for i in range(4):
    dis = calculate_dtw(histos_a[i], histos_b[i])
    print('Histo ', names[i], 'Distance: ', dis)

  cv.imshow('Comparing', np.hstack((src_a, src_b)))
  cv.waitKey(0)
  cv.destroyAllWindows()

if __name__ == '__main__':

    root = tk.Tk()
    root.withdraw()

    first_file_path = filedialog.askopenfilename()
    second_file_path = filedialog.askopenfilename()

    A = cv.imread(first_file_path, 0)
    B = cv.imread(second_file_path, 0)

    compare(A, B)
