import cv2 as cv
import numpy as np
from skimage.feature import canny

# K Means segmentation
def image_segmentation(src, clusters=3, max_iter=20):
    EPS = 0.1
    ATTEMPTS = 10
    shape = (src.shape[0] * src.shape[1])

    if len(src.shape) > 2:
        shape = (src.shape[0] * src.shape[1], src.shape[2])
    # transform image input to required shape
    src_mean = src.reshape(shape)
    src_mean = np.float32(src_mean)
    # apply k means
    criteria = (cv.TERM_CRITERIA_EPS, max_iter, EPS)
    # compactness is the sum of squared distance from each point to its center
    compactness, label, center = cv.kmeans(src_mean, clusters, None, criteria, ATTEMPTS, cv.KMEANS_RANDOM_CENTERS)
    # transorm result
    # center = np.uint8(center).flatten()
    label = label.reshape((src.shape[0], src.shape[1]))
    print('KMean:', center)

    return label, center


# Canny with int8
def sir_canny(src):
  src_canny = np.full(src.shape, 0, np.uint8)
  canny_filter = canny(src)

  for row in range(src.shape[0]):
    for col in range(src.shape[1]):
      if canny_filter[row, col]:
        src_canny.itemset((row, col), 255)

  return src_canny