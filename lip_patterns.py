import random as rng
# Basic
import cv2 as cv
import numpy as np
# Filters
from skimage.filters import sobel
# Helpers
from utils import image_segmentation, sir_canny

import tkinter as tk
from tkinter import filedialog

rng.seed(12345)
colors_kmean = [(20,153,250), (57,17,17), (122,122,122), (254,144,58), (254,66,104), (18,115,70), (242,230,177), (145,5,50)]

"""
This file will ask for two ROIs: upper and lower lip
And will show ROI + work area to get the grooves

OUTPUT
Image with grooves
Edges detected
"""


def print_to_lines(image):
    # Load source image + ROI
    src = cv.imread(image)

    if src is None:
        print('Could not open or find the image:', image)
        exit(1)

    cv.namedWindow('ROIs', cv.WINDOW_NORMAL)
    rois = cv.selectROIs('ROIs', src)
    cv.destroyAllWindows()

    if len(rois) != 2:
        print('Select 2 ROIs')
        exit(1)

    print('ROIs selected:', rois)
    roi_upper = src[int(rois[0][1]):int(rois[0][1]+rois[0][3]), int(rois[0][0]):int(rois[0][0]+rois[0][2])]
    roi_lower = src[int(rois[1][1]):int(rois[1][1]+rois[1][3]), int(rois[1][0]):int(rois[1][0]+rois[1][2])]

    process_image(roi_lower, 'lower_lip')
    process_image(roi_upper, 'upper_lip')


def select_cluster(name, values, roi, calcK=True):
    print()
    print('------------------------------------')
    print('Processing with values')
    print('# Clusters', values['clusters'])
    print('Blur window', values['blurw'])
    print('Center', values['keep_cluster'])

    src_hist = cv.cvtColor(roi, cv.COLOR_BGR2GRAY)
    src_blurred = cv.GaussianBlur(src_hist, (values['blurw'], values['blurw']), 0)

    cv.imshow('original', src_blurred)

    # Image segmentation
    src_fst_kmean = np.full((src_blurred.shape[0], src_blurred.shape[1], 3), (255, 255, 255), np.uint8)
    src_fn = np.full((src_blurred.shape[0], src_blurred.shape[1], 3), (255, 255, 255), np.uint8)
    labels, _ = values['labels'], None
    sel_cluster = values['keep_cluster']

    if calcK:
        labels, _ = image_segmentation(src_blurred, clusters=values['clusters'])
        values['labels'] = labels

    for row in range(src_blurred.shape[0]):
        for col in range(src_blurred.shape[1]):
            for i in range(3):
                src_fst_kmean.itemset((row, col, i), colors_kmean[labels[row, col]][i])
                if labels[row, col] == sel_cluster:
                    src_fn.itemset((row, col, i), 0)

    cv.imshow(name, np.hstack((src_fst_kmean, src_fn)))


def to_lines(name, values, roi):
    print()
    print('------------------------------------')
    print('Processing with values')
    print('Thin window', values['thinw'])
    print('Thick window', values['thickw'])
    print('Thin Threshold', values['thresh_thin'])
    print('Thick window', values['thresh_thick'])

    # Select cluster
    src_hist = cv.cvtColor(roi, cv.COLOR_BGR2GRAY)
    src_blurred = cv.GaussianBlur(src_hist, (values['blurw'], values['blurw']), 0)

    src_thresh = np.full((src_blurred.shape[0], src_blurred.shape[1]), 0, np.uint8)
    labels, _ = values['labels'], None
    sel_cluster = values['keep_cluster']

    for row in range(src_blurred.shape[0]):
        for col in range(src_blurred.shape[1]):
            if labels[row, col] == sel_cluster:
                src_thresh.itemset((row, col), 255)

    src_fn = cv.GaussianBlur(src_thresh, (5, 5), 0)
    # TopHat
    thin_lines = cv.morphologyEx(src_fn, cv.MORPH_BLACKHAT, np.ones((values.get('thinw'), values.get('thinw')), np.float32))
    thick_lines = cv.morphologyEx(src_fn, cv.MORPH_TOPHAT, np.ones((values.get('thickw'), values.get('thickw')), np.float32))
    ret, thin_thresh = cv.threshold(thin_lines, values.get('thresh_thin'), 255, cv.THRESH_BINARY)
    ret, thick_thresh = cv.threshold(thick_lines, values.get('thresh_thick'), 255, cv.THRESH_BINARY)

    src_tophat = np.hypot(thin_thresh, thick_thresh)
    src_tophat *= 255.0 / np.max(src_tophat)
    src_tophat = src_tophat.astype(np.uint8)
    src_tophat = np.bitwise_not(src_tophat)
    # ret, src_tophat = cv.threshold(src_tophat, 200, 255, cv.THRESH_BINARY)

    # Edge detector
    src_sobel = sobel(src_tophat)
    src_sobel_blur = cv.GaussianBlur(src_sobel, (values.get('blur_canny'), values.get('blur_canny')), 0)
    src_canny = sir_canny(src_sobel_blur)

    cv.imshow(name, src_tophat)
    cv.imwrite(name + '_tophat.png', src_tophat)
    cv.imwrite(name + '_canny.png', src_canny)
    print('------------------------------------')
    print()


def process_image(img, name):
    print()
    print('##############################################')
    print('Processing', name)
    print('Dimensions:', img.shape)

    values = {
        'blurw': 1,
        'thinw': 3,
        'thickw': 7,
        'thresh_thin': 1,
        'thresh_thick': 1,
        'blur_canny': 7,
        'clusters': 2,
        'keep_cluster': 0,
        'labels':  np.zeros((img.shape[0], img.shape[1])),
    }

    cv.namedWindow('original', cv.WINDOW_NORMAL)

    def update_nclust(v):
        values['clusters'] = v
        select_cluster(name + '_clusters', values, img)

    def update_sblurw(v):
        values['blurw'] = v * 2 + 1
        select_cluster(name + '_clusters', values, img)

    def update_selclust(v):
        values['keep_cluster'] = v
        select_cluster(name + '_clusters', values, img, False)

    cv.namedWindow(name + '_clusters', cv.WINDOW_NORMAL)
    cv.createTrackbar('Clusters', name + '_clusters', 2, 8, update_nclust)
    cv.createTrackbar('Blur', name + '_clusters', 0, 3, update_sblurw)
    cv.createTrackbar('keep_cluster', name + '_clusters', 0, 7, update_selclust)

    select_cluster(name + '_clusters', values, img)
    cv.waitKey(0)
    cv.destroyWindow(name + '_clusters')

    def update_blurw(v):
        values['blurw'] = v * 2 + 1
        to_lines(name, values, img)

    def update_thin(v):
        values['thinw'] = v * 2 + 1
        to_lines(name, values, img)

    def update_thick(v):
        values['thickw'] = v * 2 + 1
        to_lines(name, values, img)

    def update_thresh_thin(v):
        values['thresh_thin'] = v
        to_lines(name, values, img)

    def update_thresh_thick(v):
        values['thresh_thick'] = v
        to_lines(name, values, img)

    cv.namedWindow(name, cv.WINDOW_NORMAL)
    cv.createTrackbar('Thin window', name, 1, 10, update_thin)
    cv.createTrackbar('Thick window', name, 3, 6, update_thick)
    cv.createTrackbar('Thresh thin', name, 1, 255, update_thresh_thin)
    cv.createTrackbar('Thresh thick', name, 1, 255, update_thresh_thick)

    to_lines(name, values, img)
    cv.waitKey(0)
    cv.destroyAllWindows()

    print('FINAL VALUES', values)
    print('##############################################')


if __name__ == '__main__':
    root = tk.Tk()
    root.withdraw()

    file_path = filedialog.askopenfilename()
    if file_path is None:
        print('Invalid file path')
        exit(1)

    print_to_lines(file_path)
