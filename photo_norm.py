import random as rng
# Basic
import cv2 as cv
import numpy as np
# Helpers
from utils import image_segmentation

import tkinter as tk
from tkinter import filedialog

rng.seed(12345)
colors_kmean = [(20,153,250), (57,17,17), (122,122,122), (254,144,58), (254,66,104), (18,115,70), (242,230,177), (145,5,50)]


def resize_to_200(roi):
    larger = max(roi.shape[0], roi.shape[1])

    scale = 200 / larger
    new_width = int(roi.shape[1] * scale)
    new_heigth = int(roi.shape[0] * scale)

    return cv.resize(roi, (new_width, new_heigth), interpolation=cv.INTER_AREA)


def photo_to_lines(image):
    # Load source image + ROI
    src = cv.imread(image)

    if src is None:
        print('Could not open or find the image:', image)
        exit(1)

    cv.namedWindow('ROIs', cv.WINDOW_NORMAL)
    rois = cv.selectROIs('ROIs', src)
    cv.destroyAllWindows()

    if len(rois) != 2:
        print('Should select 2 ROIs')
        exit(1)

    print('ROIs selected:', rois)
    roi_upper = src[int(rois[0][1]):int(rois[0][1] + rois[0][3]), int(rois[0][0]):int(rois[0][0] + rois[0][2])]
    roi_lower = src[int(rois[1][1]):int(rois[1][1] + rois[1][3]), int(rois[1][0]):int(rois[1][0] + rois[1][2])]

    roi_upper = resize_to_200(roi_upper)
    roi_lower = resize_to_200(roi_lower)

    # Process both ROIs
    process_image(roi_lower, 'lower_lip')
    process_image(roi_upper, 'upper_lip')


def process_image(img, name):
    print()
    print('##############################################')
    print('Processing', name)
    print('Dimensions:', img.shape)

    values = {
        'hue': 255,
        'saturation': 255,
        'val': 0,
        'clusters': 3,
        'index_cluster': 0,
        'blurw': 1,
        'thinw': 1,
        'thickw': 3,
        'thresh_thin': 1,
        'thresh_thick': 1,
        'labels': np.zeros((img.shape[0], img.shape[1])),
        'center': np.zeros((3, 3))
    }

    cv.namedWindow(name + '_original', cv.WINDOW_NORMAL)

    # Step 1. Separate
    print('*********************** PART ONE')

    def update_hue(v):
        values['hue'] = v
        to_print(name, values, img)

    def update_sat(v):
        values['saturation'] = v
        to_print(name, values, img)

    def update_val(v):
        values['val'] = v
        to_print(name, values, img)

    def update_nclust(v):
        values['clusters'] = v
        to_print(name, values, img)


    cv.namedWindow(name + '_filter', cv.WINDOW_NORMAL)
    cv.createTrackbar('Hue', name + '_filter', 255, 255 * 2, update_hue)
    cv.createTrackbar('Sat', name + '_filter', 255, 255 * 2, update_sat)
    cv.createTrackbar('Value', name + '_filter', 0, 255 * 2, update_val)
    cv.createTrackbar('Clusters', name + '_filter', 3, 8, update_nclust)

    to_print(name, values, img)
    cv.waitKey(0)

    # Step 2. Get Bg
    print('*********************** PART TWO')

    def keep_i(v):
        values['index_cluster'] = v
        select_cluster(name + '_clusters', values, img)

    cv.namedWindow(name + '_clusters', cv.WINDOW_NORMAL)
    cv.createTrackbar('Keep i-cluster', name + '_clusters', 0, values['clusters'] - 1, keep_i)

    select_cluster(name + '_clusters', values, img)
    cv.waitKey(0)
    cv.destroyWindow(name + '_clusters')
    cv.destroyWindow(name + '_filter')

    # Step 3. Filters
    print('************************ PART 3')
    cv.imshow(name + '_original', img)

    def update_blurw(v):
        values['blurw'] = v * 2 + 1
        process_grooves(name + '_grooves', values, img)

    def update_thin(v):
        values['thinw'] = v * 2 + 1
        process_grooves(name + '_grooves', values, img)

    def update_thick(v):
        values['thickw'] = v * 2 + 1
        process_grooves(name + '_grooves', values, img)

    def update_thresh_thin(v):
        values['thresh_thin'] = v
        process_grooves(name + '_grooves', values, img)

    def update_thresh_thick(v):
        values['thresh_thick'] = v
        process_grooves(name + '_grooves', values, img)

    cv.namedWindow(name + '_grooves', cv.WINDOW_NORMAL)
    cv.createTrackbar('Blur window', name + '_grooves', 0, 6, update_blurw)
    cv.createTrackbar('Thin window', name + '_grooves', 0, 10, update_thin)
    cv.createTrackbar('Thick window', name + '_grooves', 1, 6, update_thick)
    cv.createTrackbar('Thresh thin', name + '_grooves', 1, 255, update_thresh_thin)
    cv.createTrackbar('Thresh thick', name + '_grooves', 1, 255, update_thresh_thick)

    process_grooves(name + '_grooves', values, img)
    cv.waitKey(0)
    cv.destroyAllWindows()

    print('FINAL VALUES', values)
    print('##############################################')


def space_color(values, roi):
    # Space color
    src_hsv = cv.cvtColor(roi, cv.COLOR_BGR2HSV)
    h, s, v = cv.split(src_hsv)

    # src_hsv[:, :, 0] = 0

    value = values['hue']
    print('Applying HUE of ', value - 255)

    if value < 255:
        lim = 255 - value
        h[h <= lim] = 0
        h[h > lim] -= lim
    else:
        vadd = value - 255
        lim = 255 - vadd
        h[h >= lim] = 255
        h[h < lim] += vadd

    value = values['saturation']
    print('Applying SATURATION of ', value - 255)

    if value < 255:
        lim = 255 - value
        s[s <= lim] = 0
        s[s > lim] -= lim
    else:
        vadd = value - 255
        lim = 255 - vadd
        s[s >= lim] = 255
        s[s < lim] += vadd

    value = values['val']
    print('Applying VALUE of ', value - 255)

    if value < 255:
        lim = 255 - value
        v[v <= lim] = 0
        v[v > lim] -= lim
    else:
        vadd = value - 255
        lim = 255 - vadd
        v[v >= lim] = 255
        v[v < lim] += vadd

    return cv.merge((h, s, v))


def to_print(name, values, roi):
    print()
    print('------------------------------------')
    print('Processing with values', values)

    src_hsv = space_color(values, roi)

    # Image segmentation
    labels, center = image_segmentation(src_hsv, clusters=values['clusters'])
    src_fst_kmean = np.full((src_hsv.shape[0], src_hsv.shape[1], src_hsv.shape[2]), (255, 255, 255), np.uint8)

    for row in range(src_hsv.shape[0]):
        for col in range(src_hsv.shape[1]):
            for i in range(3):
                src_fst_kmean.itemset((row, col, i), colors_kmean[labels[row, col]][i])

    values['labels'] = labels
    values['center'] = center
    cv.imshow(name + '_filter', src_fst_kmean)
    cv.imshow(name + '_original', src_hsv)

    print('------------------------------------')
    print()


def select_cluster(name, values, roi, show=True):
    # Apply color
    src_hsv = space_color(values, roi)

    # Image segmentation
    index_cluster = values['index_cluster']
    center = values['center']
    labels = values['labels']

    print('Keeping px with center', center[index_cluster])
    src_fst_kmean = np.full(src_hsv.shape, (255, 255, 255), np.uint8)

    for row in range(src_hsv.shape[0]):
        for col in range(src_hsv.shape[1]):
            if labels[row, col] == index_cluster:
                for i in range(3):
                    src_fst_kmean.itemset((row, col, i), 0)

    if show:
        cv.imshow(name, src_fst_kmean)

    return src_fst_kmean


def process_grooves(name, values, roi):
    print()
    print('------------------------------------')
    print('Processing with values', values)

    # Image segmentation
    src_fst_kmean = select_cluster(name, values, roi, False)

    # Apply filters
    src_blurred = cv.GaussianBlur(src_fst_kmean, (values.get('blurw'), values.get('blurw')), 0)

    thin_lines = cv.morphologyEx(src_blurred, cv.MORPH_BLACKHAT,
                                 np.ones((values.get('thinw'), values.get('thinw')), np.float32))
    thick_lines = cv.morphologyEx(src_blurred, cv.MORPH_BLACKHAT,
                                   np.ones((values.get('thickw'), values.get('thickw')), np.float32))
    ret, thin_thresh = cv.threshold(thin_lines, values.get('thresh_thin'), 255, cv.THRESH_BINARY)
    ret, thick_thresh = cv.threshold(thick_lines, values.get('thresh_thick'), 255, cv.THRESH_BINARY)

    src_tophat = np.hypot(thin_thresh, thick_thresh)
    src_tophat *= 255.0 / np.max(src_tophat)
    src_tophat = src_tophat.astype(np.uint8)
    src_tophat = np.bitwise_not(src_tophat)

    cv.imshow(name, src_tophat)
    cv.imwrite(name + '.png', src_tophat)

    print('------------------------------------')
    print()


if __name__ == '__main__':

    root = tk.Tk()
    root.withdraw()

    file_path = filedialog.askopenfilename()

    if file_path is None:
        print('Invalid image path')
        exit(1)

    photo_to_lines(file_path)
