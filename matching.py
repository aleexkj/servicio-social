import cv2 as cv
import numpy as np
import tkinter as tk
from tkinter import filedialog


def hamming_matching(des1, des2):
    bf = cv.BFMatcher(cv.NORM_HAMMING, crossCheck=True)
    # Match descriptors.
    matches = bf.match(des1, des2)
    # Distance between points, lower the better
    # for dmatch in matches:
    #   print(dmatch.distance)
    # Sort them in the order of their distance.
    matches = sorted(matches, key=lambda x: x.distance)
    # Draw first 10 matches.
    return matches[:10], dict(flags=2)


def knnMatch(des1, des2):
    bf = cv.BFMatcher()
    matches = bf.knnMatch(des1, des2, k=2)
    good = []

    for m,n in matches:
       if m.distance < 0.75*n.distance:
           good.append([m])

    return good, dict(flags=2)

def flann_match(des1, des2):
  # FLANN parameters
    FLANN_INDEX_KDTREE = 0
    index_params = dict(algorithm=FLANN_INDEX_KDTREE, trees=5)
    search_params = dict(checks=50)   # or pass empty dictionary
    flann = cv.FlannBasedMatcher(index_params,search_params)
    des1 = des1.astype(np.float32)
    des2 = des2.astype(np.float32)
    matches = flann.knnMatch(des1,des2,k=2)
  # Need to draw only good matches, so create a mask
    matchesMask = [[0,0] for i in range(len(matches))]
  # ratio test as per Lowe's paper
    for i,(m,n) in enumerate(matches):
        if m.distance < 0.7*n.distance:
            matchesMask[i]=[1,0]
    
    return matches, dict(matchesMask=matchesMask, flags=2)

def draw_kp(A, B):
    A = cv.resize(A, (B.shape[1], B.shape[0]), interpolation=cv.INTER_AREA)

    # Feature detection
    orb_A = cv.ORB_create()
    kp1, des1 = orb_A.detectAndCompute(A, None)
    kp_A = cv.drawKeypoints(A, kp1, None)

    orb_B = cv.ORB_create()
    kp2, des2 = orb_B.detectAndCompute(B, None)
    kp_B = cv.drawKeypoints(B, kp2, None)

    cv.namedWindow('Keypoints', cv.WINDOW_NORMAL)
    cv.imshow('Keypoints', np.hstack((kp_A, kp_B)))

    def draw_match(v):
        typs = [hamming_matching, knnMatch, flann_match]
        matches, draw_params = typs[v](des1, des2)

        if v >= 1:
            src_matches = cv.drawMatchesKnn(A, kp1, B, kp2, matches, outImg=None, **draw_params)
        else:
            src_matches = cv.drawMatches(A, kp1, B, kp2, matches, outImg=None, **draw_params)
    
        cv.imshow('Matching', src_matches)

    cv.namedWindow('Matching', cv.WINDOW_NORMAL)
    cv.createTrackbar('Method', 'Matching', 0, 2, draw_match)

    draw_match(0)
    cv.waitKey(0)
    cv.destroyAllWindows()


if __name__ == '__main__':

    root = tk.Tk()
    root.withdraw()

    first_file_path = filedialog.askopenfilename()
    second_file_path = filedialog.askopenfilename()

    fst = cv.imread(first_file_path, 0)
    snd = cv.imread(second_file_path, 0)

    draw_kp(fst, snd)
